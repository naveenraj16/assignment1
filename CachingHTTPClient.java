import java.io.*;
import java.net.*;

public class CachingHTTPClient {
	
	public static final String cacheDir = "/tmp/nbr329/assignment1/";
	//these 4 variables are used in many methods so they are declared globally
	//and set by the processURL method
	static URL u = null;
	static HttpURLConnection con = null;
	static String hostName= "";
	static String pathName = "";
	
	
	public static void main(String []args){
		//create the folders in the appropriate directory for the data to be stored
		createCacheFolders();
		//error if the user doesnt provide a url
		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("java CachingHTTPClient <url>");
			System.exit(0);
		}
		
		//obtain the url 
		String url = args[0];
		
		//call processURL to figure out where to get the data from, 
		//the source or the cache
		String whereFrom = processURL(url);
		//if retrieving from the source, write the data to the url and display it to console
		if(whereFrom.equals("SOURCE")){
			System.out.println("***** Serving from the source � start *****");
			writeToCacheAndDisplay(url);
			System.out.println("***** Serving from the source � end *****");
		}
		//if retrieving from cache read from the cache folder and display to console
		else if(whereFrom.equals("CACHE")){
			System.out.println("***** Serving from the cache � start *****");
			readFromCacheAndDisplay(url);
			System.out.println("***** Serving from the cache � end *****");
		}
		//if processURL doesnt return CACHE or SOURCE its an invalid url
		else{
			System.out.println("Invalid URL");
		}
	}
	
	//create the cache folder at the location stated in cacheDir
	private static void createCacheFolders(){
		File f = new File(cacheDir);
		f.mkdirs();
	}
	
	//takes a string and converts it into a unique base64 code to be used as a filename
	private static String encodeBase64(String s) throws UnsupportedEncodingException{
		byte[] randomBinaryData = s.getBytes("UTF-8");
		java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
		byte[] encoded = encoder.encode(randomBinaryData);
		return new String(encoded);
	}

	//this method reads the cached file from the url and displays to the console
	private static void readFromCacheAndDisplay(String url) {
		try {
			
			String fileName = cacheDir + hostName + "/" + encodeBase64(pathName);
			File f = new File(fileName);
 
			FileInputStream fi = new FileInputStream(f.getAbsoluteFile());
			int b = fi.read();
			//read each byte from the file and print it 
			while (b != -1) {
				System.out.print((char)b);
				b = fi.read();
			}
 
			fi.close();
 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//reads the data fromt the server and writes to the cached file
	//in addition this method also processes expires and maxage headers
	//and sets it appropriately 
	private static void writeToCacheAndDisplay(String url) {
		try {
			//find out if the header contains maxage and expires
			long timeStamp = processExpiresAndMaxageHeaders();
			long currentServerTime = con.getDate();
			byte[] b = new byte[1];
			DataInputStream di = new DataInputStream(con.getInputStream());
			String dirName = cacheDir+hostName;
			String fileName = dirName+"/"+encodeBase64(pathName);
			File dir = new File(dirName);
			//if the file does not exist in the cache, create it
			if (!dir.exists())
			  dir.mkdir();
			File file = new File(fileName);
			//if the file already exists in the cache, delete it
			if (file.exists())
			  file.delete();
			FileOutputStream fo = new FileOutputStream(file.getAbsoluteFile());
			//read each byte of the file and write it to the cache directory
			while (di.read(b,0,1) != -1) {
			  fo.write(b,0,1);
			  System.out.print((char)b[0]);
			}
			di.close();
			fo.close();
			
			//set the last modified header to the maximum of the time stamp and the 
			//current server time
			file.setLastModified(Math.max(timeStamp,currentServerTime));
 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//determines where the url is coming from
	private static String processURL(String url) {
		
		try{
			u = new URL(url);
			//get connection to obtain a response
			con = (HttpURLConnection) u.openConnection();
			hostName = u.getHost();
			pathName = u.getPath();
			if(pathName.equals(""))
				pathName = "/";
			long timeStamp = 0;
			File f = new File(cacheDir + hostName + "/" + encodeBase64(pathName));
			if(!cached(hostName,pathName))
				return "SOURCE";
			//its cached
			timeStamp = processExpiresAndMaxageHeaders();
			long currentServerTime = con.getDate();
			f.setLastModified(timeStamp);
			if(timeStamp > currentServerTime) //in guaranteed valid period
				return "CACHE";
			//create a new connection to sent information 
			con = (HttpURLConnection) u.openConnection();
			//current server time past guaranteed time
			con.setIfModifiedSince(timeStamp);
			int responseCode = con.getResponseCode();
			if(responseCode == 304)
				return "CACHE"; 
			
			return "SOURCE";
			
		}
		catch(Exception e){
			e.printStackTrace();
			con.disconnect();
			return "ERROR";
		}
	}

	//this method will try to retrieve the expires header and the cache control header
	//that may contain the maxage value, then it will create a time stamp to return
	private static long processExpiresAndMaxageHeaders() throws UnsupportedEncodingException {
		File f = new File(cacheDir + hostName + "/" + encodeBase64(pathName));
		long timeStamp = f.lastModified();
		long maxage = 0;
		long expires = 0;
        long currentServerTime = (con.getDate()); // getDate() is the current Server time in milliseconds
        expires = con.getExpiration(); // date value in epoch time - long
	    if(expires > 0)
	    	timeStamp = expires;
	    //maxage header under the "Cache-Control" field
        String str = con.getHeaderField("Cache-Control");
        //if the max age header exists
	    if(str != null){
	    	//obtain the value of the maxage using String manipulation
	    	int index = str.indexOf("max-age");
	    	if (index >= 0) {
	    		//in some cases the maxage string has a characters after the maxage value
	    		//separated by a comma
	    		int index2 = str.indexOf(",",index);
	    		//extract the value of maxage
	    		if (index2 > 0)
	    			maxage = Long.parseLong(str.substring(index + 8, index2));
	    		else
	    			maxage = Long.parseLong(str.substring(index + 8));
	    		//timeStamp is equal to the current server time plus the maxage
	    		timeStamp = currentServerTime + 1000*maxage;
	    	}
	    }
	    return timeStamp;
	}
	
	//determine if the url has already been cached or not
	private static boolean cached(String hostName, String pathName) throws UnsupportedEncodingException {
		String directoryName = cacheDir + hostName;
		String fileName = directoryName + "/" + encodeBase64(pathName);
		File f = new File(directoryName);
		if(!f.exists())
			return false;
		f = new File(fileName);
		return f.exists();
		
		
	}
}
