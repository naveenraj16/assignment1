#SUBMISSION

Naveen Rajshekhar 

nbr329

#ARCHITECTURE

##Cache Structure

Cached files are stored in /tmp/nbr329/assignment1

I create a two level heirarchy to to hold the files, the higher one to 
the host name and the lower to the url path.

Example url: http://www.cs.utexas.edu/~naveen16

host: www.cs.utexas.edu

path: /~naveen16/test/home.html

encoded(path): L35uYXZlZW4xNi90ZXN0L2hvbWUuaHRtbA== 

Absolute cached file path: /tmp/nbr329/assignment1/www.cs.utexas.edu/L35uYXZlZW4xNi90ZXN0L2hvbWUuaHRtbA==

##How do you detect a resource is available in your cache:

1. If the file corresponding to the url is not in the cache obtain it from 
the source.

2. Then we process the expires and maxage headers and timestamp the file 
with the end guaranteed period.

3. If we are in the guaranteed period, get it from the cache.

4. If it is outside of the guaranteed period, send the ifModifiedSince header
to see if the local cached copy  is still fresh. If so, get it from the cache.  

5. In all other cases return from the source. 


##How to do you handle dynamic content:

No special consideration was taken to handle dynamic content.
Dynamic pages will be retrieved form the source every time.


#REFERENCES

1. http://www.mobify.com/blog/beginners-guide-to-http-cache-headers/
2. http://www.jmarshall.com/easy/http/
3. http://www.mkyong.com/
